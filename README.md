# Formation Docker - Exercices pratiques

![Docker](./docs/img/docker-full.png "Docker")

## Pré-requis

- `docker` et `docker-compose` installés sur son poste
  - Windows : https://docs.docker.com/docker-for-windows/install/
  - Linux : https://docs.docker.com/engine/install/ubuntu/
  - Mac : https://docs.docker.com/docker-for-mac/install/
- `git` et connaissances git : https://git-scm.com/

Vérifier vos installations avec :
```
docker -v
docker-compose -v
git --version
```

### Facultatifs

- Avoir n'importe qu'elle version de dotnet core, ou la version aspnet core 5 pour utiliser l'exemple : https://dotnet.microsoft.com/download/dotnet-core (win, linux, mac)
- Plugin de Visual Studio Code pour docker et docker-compose : ms-azuretools.vscode-docker

## Exercices

A l'issue de ces exercices, vous aurez sur votre poste un environnement de développement sur des containers docker pour une application .NET, une base de données MongoDB et un Mongo Express.
Ces exercices vous permettront d'écrire un fichier docker spécifique à l'application .NET et un fichier docker-compose pour regrouper toutes les parties du projet.

### Ordre des exercices

1. docker-compose : configuration de l'image mongo : [Enoncé 01](./docs/exercices/01-docker-compose_mongo.md)
2. docker-compose : configuration de l'image mongo-express : [Enoncé 02](./docs/exercices/02-docker-compose_mongo-express.md)
3. docker-compose : création d'un stockage local : [Enoncé 03](./docs/exercices/03-docker-compose_stockage_local.md)
4. docker : conteneurisation de l'application C# : [Enoncé 04](./docs/exercices/04-docker_containeriser_application.md)
5. docker-compose : Ajout de l'application C# dans le docker-compose : [Enoncé 05](./docs/exercices/05-docker-compose_application.md)
6. docker-compose : Création d'un réseau virtuel privé : [Enoncé 06](./docs/exercices/06-docker-compose_reseau_virtuel_prive.md)

### Démarrer

1. Faites un fork de ce projet
2. Lire l'énoncé de l'exercice que vous souhaitez faire et commencer à implémenter :)
   - Si vous souhaitez faire les exercices dans l'ordre, commencez vos implémentations sur une nouvelle branche à partir de l'initialisation du premier exercice : `git checkout -b ma-branche init-1`
   - Sinon, commencez vos implémentations sur une nouvelle branche à partir de l'initialisation de l'exercice souhaité : `git checkout -b ma-branche init-<no de l'exercice>`

Chaque exercice a un énoncé détaillé dans un fichier dans le répertoire `docs/exercices`.

Vous pouvez accèder à l'état initial de chaque exercice avec un `git checkout init-<no de l'exercice>`.

Vous pouvez accèder à une solution de chaque exercice avec un `git checkout solution-<no de l'exercice>`.

Vous pouvez voir les différentes étapes de chaque exercice en consultant les modifications apportées à chaque commit.

Il est fort possible que vous implémentiez des solutions un peu différentes des solutions proposées mais qui répondent très bien au besoin.

## Aides et documentations

Pour tester son fichier `docker-compose.yml` :
```
# monter et démarrer le(s) container(s) du fichier :
docker-compose up -d
# monter et démarrer le(s) container(s) du fichier en forçant le build des containers créés depuis un Dockerfile:
docker-compose up --build -d
# stopper le(s) container(s) du fichier en supprimant les volumes associés :
docker-compose down --volumes
# stopper le(s) container(s) du fichier en concervant les volumes associés :
docker-compose down
```

Vous pouvez voir les logs (sortie console) des containers dans l'interface Docker Desktop. Tous les containers d'un même docker-compose sont regroupés, vous pouvez déplier le groupe pour voir le détails de chaque container.

![Containers dans le groupe (docker v20.10)](./docs/img/docker_desktop_containers.PNG "Containers dans le groupe (docker v20.10)")

Chaque fiche de détails des exercices précise des sources pour vous aider à réaliser l'exercice.

Commandes utiles pour nettoyer son environnement docker :
```
# Supprimer tous les réseaux non utilisés
docker network prune
# Supprimer tous les volumes non utilisés
docker volume prune
# Supprimer toutes les images
docker image prune -a
```

Vous trouverez les documentations officielles de docker et docker-compose sur https://docs.docker.com/

## Aller plus loin

Pour poursuivre votre montée en compétence sur docker, je vous invite à regarder les tutoriels et projets samples mis à disposition par docker : https://docs.docker.com/samples/
