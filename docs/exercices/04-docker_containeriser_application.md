# Containeriser une application

## Objectifs

Ecrire un fichier `application/Dockerfile` pour créer un container docker qui exécute l'application pour interroger la base de données Mongo, avec les détails suivants :
- le Dockerfile doit utiliser l'image `mcr.microsoft.com/dotnet/sdk:5.0` comme image de base
- l'application doit être exécutée automatiquement au lancement du docker
- l'application doit afficher la liste des livres présents dans la base `books` de mongoDB
- l'application se connecte à la base de données mongoDB du docker-compose
- le container de l'application n'est **pas** inclus dans le docker-compose

Vous pouvez modifier les sources de l'application pour établir la connexion à la base de données.

## Aides et documentations

### docker

- Dockerfile : https://docs.docker.com/engine/reference/builder/
- image docker de .NET SDK : https://hub.docker.com/_/microsoft-dotnet-sdk/
- builder une nouvelle image : https://docs.docker.com/engine/reference/commandline/build/
- démarrer un container à partir d'une image : https://docs.docker.com/engine/reference/commandline/run/
  - en précisant des variables d'environnement : https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file
- Comment faire pour qu'un container puisse interroger un service de son host : https://docs.docker.com/docker-for-windows/networking/#use-cases-and-workarounds

### .NET

- Exécution du code source : https://docs.microsoft.com/fr-fr/dotnet/core/tools/dotnet-run
- Obtenir la valeur d'une variable d'environnement : https://docs.microsoft.com/fr-fr/dotnet/api/system.environment.getenvironmentvariable?view=net-5.0

### application

Les sources de l'application sont dans le répertoire `application`.

Cette application permet une gestion minimisée d'une liste de livres. La liste des livres est stockée dans une base de données mongoDB.

Il s'agit d'une application exécutable en ligne de commande avec les possibilités suivantes :
- ajouter un livre à la liste : `add <TITRE> <PRIX>`
- afficher un message d'aide : `--help`
- afficher la liste de tous les livres : aucun paramètre

Dans les sources, la connexion à la base de données est implémentée dans le fichier `Program.cs`.