# Configurer l'application dans le docker-compose

## Objectifs

Ajouter dans le fichier `docker-compose.yml` un container avec l'image la plus récente de l'application, avec les détails suivants :
- le nom du service définissant le container doit être `app`
- le nom du container doit être `app`
- l'image docker doit être buildée à partir du docker-compose et son nom sera `mongo_dotnet_app`

## Aides et documentations

### docker compose

- https://docs.docker.com/compose/
- Configurer le build d'un docker dans docker-compose : https://docs.docker.com/compose/compose-file/compose-file-v3/#build