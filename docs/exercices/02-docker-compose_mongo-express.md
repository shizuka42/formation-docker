# Configurer mongo-express dans le docker-compose

## Objectifs

Ajouter dans le fichier `docker-compose.yml` un container avec l'image la plus récente de mongo-express, avec les détails suivants :
- le nom du service définissant le container doit être `mongo-express`
- le nom du container doit être `mongo-express`
- l'utilisateur admin utilisé par mongo-express doit être celui de la base de données mongo (username et mot de passe) 
- le port d'accès à mongo-express doit être exposé (accessible depuis le host)
- l'interface mongo-express doit être fonctionnelle depuis le host en requêtant http://localhost:8081/

## Aides et documentations

### docker compose

- https://docs.docker.com/compose/
- réseau entre les containers d'un même compose : https://docs.docker.com/compose/networking/

### Mongo-express

- Docker Hub, image mongo-express : https://registry.hub.docker.com/_/mongo-express