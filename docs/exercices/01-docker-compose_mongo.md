# Configurer mongoDB dans un docker-compose

## Objectifs

Ecrire un fichier `docker-compose.yml` qui met en place un container avec l'image la plus récente de mongoDB, avec les détails suivants :
- le nom du service définissant le container doit être `database`
- le nom du container doit être `mongoDB`
- l'utilisateur root de mongo doit être `db_admin` et son mot de passe `db_pwd`
- le port d'accès à mongo doit être exposé (accessible depuis le host)

## Aides et documentations

### docker compose

- https://docs.docker.com/compose/

### Docker Hub

- image mongo : https://hub.docker.com/_/mongo
- tag par défaut : https://docs.docker.com/engine/reference/commandline/pull/#pull-an-image-from-docker-hub

### Configuration simple des containers

- Ajouter un nom précis à chaque container : https://docs.docker.com/compose/compose-file/compose-file-v3/#container_name
- Définition des ports exposés : https://docs.docker.com/compose/compose-file/compose-file-v3/#ports

### Variables d'environnement

- Utiliser des variables d'environnement : https://docs.docker.com/compose/compose-file/compose-file-v3/#environment
- Utiliser un fichier de variables d'environnement : https://docs.docker.com/compose/environment-variables/ et https://docs.docker.com/compose/compose-file/compose-file-v3/#env_file