# Utilisation de fichier local et volume

## Objectifs

- Utiliser le fichier `mongo/init-mongo.js` pour initialiser la base de données mongo.
- Déclarer un volume pour la base de données mongo

## Aides et documentations

### docker

- Volumes : https://docs.docker.com/storage/volumes/

### docker compose

- https://docs.docker.com/compose/
- Volumes : https://docs.docker.com/compose/compose-file/compose-file-v3/#volume-configuration-reference et https://docs.docker.com/compose/compose-file/compose-file-v3/#volumes

### Mongo

- Initialisation d'une base mongo : voir la partie "Initializing a fresh instance" de https://hub.docker.com/_/mongo
