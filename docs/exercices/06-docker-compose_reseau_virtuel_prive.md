# Création d'un réseau virtuel privé

## Objectifs

Mettre en place dans le fichier `docker-compose.yml` un réseau virtuel privé pour que seuls les containers de l'application et mongo-express puissent avoir accès à la base de données mongo.

## Aides et documentations

### docker compose

- https://docs.docker.com/compose/
- Utilisation et déclaration des réseaux : https://docs.docker.com/compose/compose-file/compose-file-v3/#networks